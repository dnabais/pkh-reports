from cx_Freeze import setup, Executable

# python setup.py build (200mb. Needs another option for smaller package)
setup(name = "pkhReportMaker",
      version = "1.2",
      description = "",
      executables = [Executable("reportMakerPyQt5.py")])


# from distutils.core import setup
# import py2exe

# # python setup.py py2exe --includes sip 
# # setup(
# #     options={"py2exe":{"dll_excludes":"MSVCP90.dll"}},
# #     windows=['reportMakerPyQt5.py']
# # )
# setup(console=['reportMakerPyQt5.py'])
