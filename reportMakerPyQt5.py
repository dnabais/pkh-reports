import sqlite3
import string
import datetime
import sys, traceback
# from PyQt5 import QtGui, QtCore
from PyQt5 import QtCore, QtGui, QtWidgets
from json import dumps

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

connection = sqlite3.connect('localDB.db')
c = connection.cursor()

c.execute(''' CREATE TABLE IF NOT EXISTS properties (id numeric unique, code text unique) ''')
c.execute(''' CREATE TABLE IF NOT EXISTS rooms (id numeric unique, name text, propertyCodeId numeric) ''')

c.execute(''' INSERT OR IGNORE INTO properties (id, code) VALUES (1, "MY") ''')
c.execute(''' INSERT OR IGNORE INTO properties (id, code) VALUES (2,"SP") ''')
c.execute(''' INSERT OR IGNORE INTO properties (id, code) VALUES (3,"TI") ''')
          
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (1, "2 Bedroom Oceanfront Casita Suite", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (2, "2 Bedroom Oceanfront Suite", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (3, "Bambu Suite", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (4, "Bambu Suite - RM 2", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (5, "Deluxe Casita Room", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (6, "Fairmont Deluxe Room", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (7, "Fairmont Room", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (8, "Signature Casita Room", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (9, "Signature Casita Suite", 1) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (10, "Casita Suite", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (11, "Fairmont", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (12, "Fairmont Deluxe", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (13, "Master Suite", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (14, "Master Suite - 2nd Bedroom", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (15, "Presidential Suite", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (16, "Signature Room", 2) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (17, "Hibiscus Dlx GlfVw", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (18, "Hibiscus King", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (19, "Hibiscus Suite", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (20, "Jasmine Dlx", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (21, "Jasmine Dlx GlfVw", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (22, "Jasmine Suite", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (23, "Magnolia Dlx GlfVw", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (24, "Magnolia Suite", 3) ''')
c.execute(''' INSERT OR IGNORE INTO rooms (id, name, propertyCodeId) VALUES (25, "Orchid GlfVw", 3) ''')

connection.commit()

dbLoaded = False
def initDB(filename):
    global dbLoaded
    dbLoaded = False
    c.execute('DROP TABLE IF EXISTS contacts')
    c.execute('''
    CREATE TABLE contacts
    (first_name text, last_name text, arrival_date numeric, departure_date numeric, age integer, id_status text, property_code text, room_category text, room_lead text);
    ''')

    dataFile = open(filename, 'r')
    dataFile.readline()
    contactList = []
    for line in dataFile:
        fName,lName,aDate,dDate,age,idStatus,pCode,rCategory,roomLead = line.split(',')
        fName = fName[1:-1]
        lName = lName[1:-1]
        if aDate[1:-1] != "":
            aDate = aDate[1:-1].split('/')
            if len(aDate[0]) == 1: aDate[0] = '0%s' % aDate[0] # zero padding
            if len(aDate[1]) == 1: aDate[1] = '0%s' % aDate[1] # zero padding
            aDate = "%s-%s-%s" % (aDate[2], aDate[0], aDate[1]) # YYYY-MM-DD
        else: aDate = aDate[1:-1]
        if dDate[1:-1] != "":
            dDate = dDate[1:-1].split('/')
            if len(dDate[0]) == 1: dDate[0] = '0%s' % dDate[0] # zero padding
            if len(dDate[1]) == 1: dDate[1] = '0%s' % dDate[1] # zero padding
            dDate = "%s-%s-%s" % (dDate[2], dDate[0], dDate[1]) # YYYY-MM-DD
        else: dDate = dDate[1:-1]
        age = age[1:-1]
        try:
            age = int(age)
            if age > 18: age = 18
        except ValueError:
            if age == 'Adult': age = 18
            if age == 'N': age = 99
        idStatus = idStatus[1:-1]
        pCode = pCode[1:-1]
        rCategory = rCategory[1:-1]
        roomLead = ''.join(c for c in roomLead.upper() if c in string.ascii_uppercase)
        if roomLead != 'Y': roomLead = 'N'
        contactList.append((fName, lName, aDate, dDate, age, idStatus, pCode, rCategory, roomLead))
    dataFile.close()
    c.executemany("INSERT INTO contacts VALUES (?,?,?,?,?,?,?,?,?)", contactList)
    connection.commit()
    dbLoaded = True

def createCSV(beginDate, endDate, propertyCode, outputFilename, ageIntervalSelected):
    firstDay = datetime.datetime.strptime(beginDate, "%m/%d/%Y") # MM/DD/YYYY
    currDay = firstDay
    lastDay = datetime.datetime.strptime(endDate, "%m/%d/%Y")
    dayList = []
    while currDay <= lastDay:
        dayList.append(currDay)
        currDay += datetime.timedelta(days=1)

    roomLists = {propertyCode:[]}
    c.execute('select id from properties where code = ?', (propertyCode, ))
    codeId = c.fetchone()[0]
    c.execute('select name from rooms where propertyCodeId = ?', (codeId, ))
    for room in c:
        roomLists[propertyCode].append(room[0])
    
    if ageIntervalSelected == 0:
        ageIntervals = [(0,2),
                        (3,5),
                        (6,9),
                        (10,13),
                        (14,17),
                        (18,18),
                        (99,99)]
    else:
        ageIntervals = [(0,2), (3,12), (13,18), (99,99)]

    csvFile = open(outputFilename, "w")
    csvFile.write("Property: %s\nFrom: %s\nTo: %s\n" % (propertyCode, beginDate, endDate))
    csvFile.write("Room Counts\n")
    csvLine = ""
    for d in dayList:
        csvLine += ",%d" % d.day
    csvFile.write(csvLine + "\n")
    for room in roomLists[propertyCode]:
        csvLine = "%s" % room
        for d in dayList:
            c.execute("select count(*) from contacts where room_category = ? and arrival_date <= ? and departure_date > ? and property_code=?", (room, d.date(), d.date(), propertyCode))
            for r in c:
                if r[0] == 0: csvLine += ","
                else: csvLine += ",%d" % r[0]
        csvFile.write(csvLine + "\n")

    csvFile.write("\n")
    csvFile.write("Staff Room Breakdown\n")
    for room in roomLists[propertyCode]:
        csvLine = "%s" % room
        for d in dayList:
            c.execute("select count(*) from contacts where id_status like '%staff' and room_category = ? and arrival_date <= ? and departure_date > ? and property_code=?", (room, d.date(), d.date(),propertyCode))
            for r in c:
                if r[0] == 0: csvLine += ","
                else: csvLine += ",%d" % r[0]
        csvFile.write(csvLine + "\n")

    csvFile.write("\n")
    csvFile.write("Head Counts\n")
    csvLine = ""
    for d in dayList:
        csvLine += ",%d" % d.day
    csvFile.write(csvLine + "\n")
    
    for (lower, upper) in ageIntervals:
        if upper == 18: 
            csvLine = "%d years old & up" % lower
        else: 
            if upper == 99: 
                csvLine = "N = Aides"
            else: 
                csvLine = "%d-%d years old" % (lower, upper)
        for d in dayList:
            c.execute("select count(*) from contacts where arrival_date <= ? and departure_date > ? and property_code=? and age between ? and ?", (d.date(), d.date(), propertyCode, lower, upper))
            for r in c:
                if r[0] == 0: csvLine += ","
                else: csvLine += ",%d" % r[0]
        csvFile.write(csvLine + "\n")

    csvFile.write("\n")
    csvLine = ""
    for d in dayList:
        csvLine += ",%d" % d.day
    csvFile.write(csvLine + "\n")

    for staff in ['Staff','Seated Staff']:
        csvLine = "%s" % staff
        for d in dayList:
            c.execute("select count(*) from contacts where arrival_date <= ? and departure_date > ? and property_code=? and id_status = ?", (d.date(), d.date(), propertyCode, staff))
            for r in c:
                if r[0] == 0: csvLine += ","
                else: csvLine += ",%d" % r[0]
        csvFile.write(csvLine + "\n")

#connection.close()

managerWindow = None
class RoomWindow(QtWidgets.QTableWidget):
    def __init__(self, *args):
        QtWidgets.QTableWidget.__init__(self, *args)
        self.setData()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.move(10,50)
        
    def setData(self):
        c2 = connection.cursor()
        c.execute("select id, code from properties")
        line = 0
        self.insertColumn(0)
        self.insertColumn(1)
        for p in c:
            c2.execute("select name from rooms where propertyCodeId = ?", (str(p[0]), ))
            for r in c2:
                self.insertRow(self.rowCount())
                self.setItem(line, 0, QtWidgets.QTableWidgetItem(p[1]))
                self.setItem(line, 1, QtWidgets.QTableWidgetItem(r[0]))
                line += 1

class MainWindow(QtWidgets.QMainWindow):#, UI.MainUI.Ui_MainWindow):

    beginDate = ""
    endDate = ""
    propertyCode = "MY"
    ageInterval = 0

    def __init__(self):
        super(MainWindow, self).__init__()
        self.initUI()
        
    def initUI(self):
        
        self.setGeometry(100, 100, 750, 550)
        self.setWindowTitle('Report Maker')

        helpbtn = QtWidgets.QPushButton('Help', self)
        helpbtn.clicked.connect(self.showHelp)
        helpbtn.resize(helpbtn.sizeHint())
        helpbtn.move(10, 11)

        self.combo = QtWidgets.QComboBox(self)
        c.execute("select code from properties")
        for p in c:
            self.combo.addItem(p[0])
        self.combo.move(100, 10)
        self.combo.activated[str].connect(self.onActivated)

        self.beginDatelbl = QtWidgets.QLabel(self)
        self.beginDatelbl.setText("First Day")
        self.beginDatelbl.move(10,80)
        beginDateCal = QtWidgets.QCalendarWidget(self)
        beginDateCal.setGridVisible(True)
        beginDateCal.move(10, 100)
        beginDateCal.clicked[QtCore.QDate].connect(self.setBeginDate)
        beginDateCal.setFixedWidth(320)
        beginDateCal.setFixedHeight(240)

        self.endDatelbl = QtWidgets.QLabel(self)
        self.endDatelbl.setText("Last Day")
        self.endDatelbl.move(400, 80)
        endDateCal = QtWidgets.QCalendarWidget(self)
        endDateCal.setGridVisible(True)
        endDateCal.move(400, 100)
        endDateCal.clicked[QtCore.QDate].connect(self.setEndDate)
        endDateCal.setFixedWidth(320)
        endDateCal.setFixedHeight(240)

        self.outputlbl = QtWidgets.QLabel(self)
        self.outputlbl.setText("Output file name")
        self.outputlbl.move(10,500)

        self.outputTextField = QtWidgets.QLineEdit(self)
        self.outputTextField.move(10, 520)
        self.outputTextField.setText("MY.csv")

        qbtn = QtWidgets.QPushButton('Create Report', self)
        qbtn.clicked.connect(self.callCreateCSV)
        qbtn.resize(qbtn.sizeHint())
        qbtn.move(600, 520)

        self.dblbl = QtWidgets.QLabel(self)
        self.dblbl.setText("No DB dump selected...")
        self.dblbl.move(400,40)
        selectDBbtn = QtWidgets.QPushButton('Select DB dump', self)
        selectDBbtn.clicked.connect(self.showDialog)
        selectDBbtn.resize(qbtn.sizeHint())
        selectDBbtn.move(400, 10)

        self.ageSelectorlbl = QtWidgets.QLabel(self)
        self.ageSelectorlbl.setText("Age intervals")
        self.ageSelectorlbl.move(10,340)

        ageIntervalSelector = QtWidgets.QComboBox(self)
        ageIntervalSelector.addItem("[0,2];[3,5];[6,9];[10,13];[14,17];[18,...] + Aides")
        ageIntervalSelector.addItem("[0,2];[3,12];[13,...] + Aides")
        ageIntervalSelector.move(10, 360)
        ageIntervalSelector.activated[str].connect(self.onAgeSelected)

        managebtn = QtWidgets.QPushButton('List Rooms', self)
        managebtn.clicked.connect(self.showRoomWindow)
        managebtn.resize(managebtn.sizeHint())
        managebtn.move(500, 340)

        addPbtn = QtWidgets.QPushButton('Add Property Code', self)
        addPbtn.clicked.connect(self.addPropertyCode)
        addPbtn.resize(addPbtn.sizeHint())
        addPbtn.move(500, 370)

        rmPbtn = QtWidgets.QPushButton('Remove Property Code', self)
        rmPbtn.clicked.connect(self.removePropertyCode)
        rmPbtn.resize(rmPbtn.sizeHint())
        rmPbtn.move(500, 400)

        addRbtn = QtWidgets.QPushButton('Add Room', self)
        addRbtn.clicked.connect(self.addRoom)
        addRbtn.resize(addRbtn.sizeHint())
        addRbtn.move(500, 430)

        rmRbtn = QtWidgets.QPushButton('Remove Room', self)
        rmRbtn.clicked.connect(self.removeRoom)
        rmRbtn.resize(rmRbtn.sizeHint())
        rmRbtn.move(500, 460)

        # Defaults
        date = beginDateCal.selectedDate()
        self.beginDate = "%02d/%02d/%d" % (date.month(),date.day(),date.year())
        date = endDateCal.selectedDate()
        self.endDate = "%02d/%02d/%d" % (date.month(),date.day(),date.year())

        self.show()

    def updateCombo(self):
        c.execute("select count(*) from properties")
        for i in xrange(c.fetchone()[0]):
            self.combo.removeItem(0)
        c.execute("select code from properties")
        for p in c:
            self.combo.addItem(p[0])

    def addPropertyCode(self):
        text, ok = QtWidgets.QInputDialog.getText(self, "Adding Property Code", "What is the property code you want to add?")
        if ok:
            c.execute("select max(id) from properties")
            newId = int(c.fetchone()[0]) + 1
            c.execute("insert into properties (id, code) values (?, ?)" , (newId, str(text)))
            connection.commit()
            self.updateCombo()

    def removePropertyCode(self):
        c.execute("select code from properties")
        text, ok = QtWidgets.QInputDialog.getItem(self, "Remove Property Code",
    "Which property code do you want to remove?", [p[0] for p in c]    )
        if ok:
            c.execute("delete from properties where code = ?", (str(text), ))
            connection.commit()
            self.updateCombo()

    def addRoom(self):
        c.execute("select code from properties")
        text, ok = QtWidgets.QInputDialog.getItem(self, "Add Room",
    "To which property code do you want to add a room?", [p[0] for p in c]    )
        if ok:
            c.execute("select max(id) from rooms")
            roomCount = int(c.fetchone()[0]) + 1
            c.execute("select id from properties where code = ?", (str(text), ))
            pCode = c.fetchone()[0]
            text, ok = QtWidgets.QInputDialog.getText(self, "Add Room", "What is the room name?")
            if ok:
                c.execute("insert into rooms (id, name, propertyCodeId) values (?,?,?)", (roomCount, str(text), pCode))
                connection.commit()

    def removeRoom(self):
        c.execute("select code from properties")
        text, ok = QtWidgets.QInputDialog.getItem(self, "Remove Room",
    "From which property code do you want to remove a room?", [p[0] for p in c]    )
        if ok:
            c.execute("select id from properties where code = ?", (str(text), ))
            pCode = c.fetchone()[0]
            c.execute("select name from rooms where propertyCodeId = ?", (pCode, ))
            text, ok = QtWidgets.QInputDialog.getItem(self, "Remove Room", "What is the room name?", [p[0] for p in c])
            if ok:
                c.execute("delete from rooms where propertyCodeId = ? and name = ?", (pCode, str(text)))
                connection.commit()

    def showRoomWindow(self):
        global managerWindow
        managerWindow = RoomWindow()
        managerWindow.show()

    def onAgeSelected(self, text):
        if len(str(text)) > 30: self.ageInterval = 0
        else: self.ageInterval = 1

    def showHelp(self):
        helpmsg = """
On pkh.wirecontact.com go to the admin section -> trouble shooting -> SQL and run the following query:

select FirstName, LastName, ArrivalDate, DepartureDate, Age, IDStatus, PropertyCode, Room_Category, Isthistheroomlead from wce_contact where N2014 != ''

Then save the file with:
        First row filed labels: yes
        Field Delimiter: , (comma)
        Quote Fields: Yes
"""
        helpDialog = QtWidgets.QInputDialog(self)
        helpDialog.setInputMode(QtWidgets.QInputDialog.TextInput)
        helpDialog.setLabelText(helpmsg)
        helpDialog.setTextValue("select FirstName, LastName, ArrivalDate, DepartureDate, Age, IDStatus, PropertyCode, Room_Category, Isthistheroomlead from wce_contact where N2014 != ''")
        helpDialog.show()

    def showDialog(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Select DB dump', '.')[0]
        try:
            initDB(fname)
            self.dblbl.setText("%s loaded" % fname) 
        except:
            QtWidgets.QMessageBox.critical(self, 'Error', 'The file seems invalid...', QtWidgets.QMessageBox.Ok)
            errorFile = open('error.log','w')
            traceback.print_exc(file=errorFile)

    def callCreateCSV(self):
        if not dbLoaded: QtWidgets.QMessageBox.critical(self, 'Error', 'Please select a valid dbDump file', QtWidgets.QMessageBox.Ok)
        else:
            createCSV(self.beginDate, self.endDate, self.propertyCode, self.outputTextField.text(), self.ageInterval)
            QtWidgets.QMessageBox.information(self, 'Success', 'Report written to ' + self.outputTextField.text(), QtWidgets.QMessageBox.Ok)

    def onActivated(self, text):
        self.propertyCode = str(text)
        self.outputTextField.setText(str(text) + ".csv")
        
    def setBeginDate(self, date):
        self.beginDate = "%02d/%02d/%d" % (date.month(),date.day(),date.year())
        
    def setEndDate(self, date):
        self.endDate = "%02d/%02d/%d" % (date.month(),date.day(),date.year())

def main():
    global managerWindow
    app = QtWidgets.QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
